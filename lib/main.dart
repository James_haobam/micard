import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MiCard',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MiCardPage(title: 'MiCard'),
    );
  }
}

class MiCardPage extends StatefulWidget {
  const MiCardPage({super.key, required this.title});

  final String title;

  @override
  State<MiCardPage> createState() => _MiCardPageState();
}

class _MiCardPageState extends State<MiCardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.brown[800],
        appBar: AppBar(
          elevation: 0.7,
          backgroundColor: Colors.brown[800],
          title: Text(
            widget.title,
            style: const TextStyle(color: Colors.white, fontFamily: "ACLONICA"),
          ),
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage('images/emoji.jpg'),
              ),
              const SizedBox(
                height: 30,
              ),
              _text(
                label: "James Haobam",
                font_familty: "ACLONICA",
              ),
              _text(
                  label: "Software Engineer",
                  font_familty: "ACTOR",
                  size: 17.0),
              const SizedBox(
                height: 15.0,
                width: 100,
                child: Divider(
                  color: Colors.white,
                ),
              ),
              _container(
                label: "+91432522323",
                icon: Icons.phone_android,
              ),
              _container(
                  label: "jameskumar@agobyte.com",
                  icon: Icons.email,
                  size: 13.5),
              _container(label: "Nagamapal", icon: Icons.location_city),
            ],
          ),
        ));
  }

  Card _container(
      {required String label, required IconData icon, double size = 17.0}) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      child: ListTile(
        leading: Icon(
          icon,
          color: Colors.brown,
        ),
        title: Text(
          label,
          style: TextStyle(color: Colors.brown, fontSize: size),
        ),
      ),
    );
  }

  Text _text(
      {required String label,
      required String font_familty,
      double size = 20.0}) {
    return Text(
      label,
      style: TextStyle(
          fontSize: size,
          fontWeight: FontWeight.w100,
          color: Colors.white,
          fontFamily: font_familty),
    );
  }
}
